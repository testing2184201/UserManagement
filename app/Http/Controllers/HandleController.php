<?php
namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class HandleController extends Controller
{
    public function index($more = 'Something')
    {

        return User::all();
    }
    public function create(Request $request)
    {

        $this->validate($request, [
            'name' => 'required',
            'age' => 'required|gte:0',
            'country' => 'required',
        ]);
        User::create([
            'name' => $request->name,
            'age' => $request->age,
            'country' => $request->country,
        ]);
        return response()->json(['status' => 'true', 'message' => 'User created successfully'], 201);
    }
    public function update(Request $request)
    {

        $this->validate($request, [
            'id_update' => 'required|exists:users,id',
            'age' => 'gte:18',
        ]);
        $data_update = [];
        $request->name ? $data_update['name'] = $request->name : '';
        $request->age ? $data_update['age'] = $request->age : '';
        $request->country ? $data_update['country'] = $request->country : '';
        User::find($request->id_update)->update($data_update);
        return response()->json(['status' => 'true', 'message' => 'User successfully updated'], 201);
    }
    public function delete(Request $request)
    {

        $this->validate($request, [
            'id_delete' => 'required|exists:users,id',
        ]);
        User::find($request->id_delete)->delete();
        return response()->json(['status' => 'true', 'message' => 'User successfully deleted'], 201);
    }
}
